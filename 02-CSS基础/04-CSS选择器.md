

## CSS的四种基本选择器

CSS选择器：就是指定CSS要作用的标签，那个标签的名称就是选择器。意为：选择哪个容器。

CSS的选择器分为两大类：基本选择题和扩展选择器。

**基本选择器：**

- 标签选择器：针对**一类**标签
- ID选择器：针对某**一个**特定的标签使用
- 类选择器：针对**你想要的所有**标签使用
- 通用选择器（通配符）：针对所有的标签都适用（不建议使用）

下面来分别讲一讲。

### 1、标签选择器：选择器的名字代表html页面上的标签

标签选择器，选择的是页面上所有这种类型的标签，所以经常描述“**共性**”，无法描述某一个元素的“个性”。

举例：

```html
p{
	font-size:14px;
}
```

上方选择器的意思是说：所有的`<p>`标签里的内容都将显示14号字体。

效果：

![](http://img.smyhvae.com/2015-10-03-css-06.png)

再比如说，我想让“生命壹号学完了安卓，继续学前端哟”这句话中的“前端”两个变为红色字体，那么我可以用`<span>`标签把“前端”这两个字围起来，然后给`<span>`标签加一个标签选择器。

代码如下：

```html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<style type="text/css">
		span{
			color: red;
		}
	</style>
</head>
<body>
	<p>生命壹号学完了安卓，继续学<span>前端</span>哟</p>
</body>
</html>
```

【总结】需要注意的是：

（1）所有的标签，都可以是选择器。比如ul、li、label、dt、dl、input。

（2）无论这个标签藏的多深，一定能够被选择上。

（3）选择的所有，而不是一个。

### 2、ID选择器：规定用`#`来定义

针对某一个特定的标签来使用，只能使用一次。css中的ID选择器以”#”来定义。

举例：

```html
#mytitle{
	border:3px dashed green;
}
```
效果：

![](http://img.smyhvae.com/2015-10-03-css-08.png)

id选择器的选择符是“#”。

任何的HTML标签都可以有id属性。表示这个标签的名字。这个标签的名字，可以任取，但是：

- （1）只能有字母、数字、下划线。
- （2）必须以字母开头。
- （3）不能和标签同名。比如id不能叫做body、img、a。

另外，特别强调的是：**HTML页面，不能出现相同的id，哪怕他们不是一个类型**。比如页面上有一个id为pp的p，一个id为pp的div，是非法的！

**一个标签可以被多个css选择器选择：**

比如，我们可以同时让标签选择器和id选择器作用于同一个标签。如下：

![](http://img.smyhvae.com/20170710_1737.png)

然后我们通过网页的审查元素看一下效果：

![](http://img.smyhvae.com/20170711_1540.png)

现在，假设选择器冲突了，比如id选择器说这个文字是红色的，标签选择器说这个文字是绿色的。那么听谁的？
实际上，css有着非常严格的计算公式，能够处理冲突.

一个标签可以被多个css选择器选择，共同作用，这就是“**层叠式**”的第一层含义（第一层含义和第二层含义，放到css基础的第三篇文章里讲）。

### 3、类选择器：规定用圆点`.`来定义

针对**你想要的所有**标签使用。优点：灵活。


css中用`.`来表示类。举例如下：

```html
.one{
	width:800px;
}

```

效果：

![](http://img.smyhvae.com/2015-10-03-css-07.png)


和id非常相似，任何的标签都可以携带id属性和class属性。class属性的特点：

- 特性1：类选择器可以被多种标签使用。

- 特性2：同一个标签可以使用多个类选择器。用**空格**隔开。举例如下：（正确）

```html
<h3 class="teshu  zhongyao">我是一个h3啊</h3>
```

初学者常见的错误，就是写成了两个class。举例如下：（错误）

```html
<h3 class="teshu" class="zhongyao">我是一个h3啊</h3>
```

**类选择器使用的举例：**

类选择器的使用，能够决定一个人的css水平。

比如，我们现在要做下面这样一个页面：

![](http://img.smyhvae.com/20170711_1639.png)

正确的思路，就是用所谓“公共类”的思路，就是我们类就是提供“公共服务”，比如有绿、大、线，一旦携带这个类名，就有相应的样式变化。对应css里的代码如下：

```html
	<style type="text/css">
		.lv{
			color: green;
		}
		.da{
			font-size: 30px;
		}
		.underline{
			text-decoration: underline;
		}
	</style>
```

然后让每个标签去选取自己想要用的类选择器：

```html
  <p class="lv da">段落1</p>
	<p class="lv xian">段落2</p>
	<p class="da xian">段落3</p>
```

也就是说：

（1）不要去试图用一个类名，把某个标签的所有样式写完。这个标签要多携带几个类，共同完成这个标签的样式。

（2）每一个类要尽可能小，有“公共”的概念，能够让更多的标签使用。

问题：到底用id还是用class？

答案：尽可能的用class，除非极特殊的情况可以用id。

原因：id是js用的。也就是说，js要通过id属性得到标签，所以css层面尽量不用id，要不然js就很别扭。另一层面，我们会认为一个有id的元素，有动态效果。

举例如下：

![](http://img.smyhvae.com/20170711_1706.png)

上图所示，css和js都在用同一个id，会出现不好沟通的情况。

我们记住这句话：**类上样式，id上行为**。意思是说，`class`属性交给css使用，`id`属性交给js使用。

**上面这三种选择器的区别：**

- 标签选择器针对的是页面上的一类标签。
- ID选择器是只针对特定的标签(一个)，ID是此标签在此页面上的唯一标识。
- 类选择器可以被多种标签使用。

### 4、通配符`*`：匹配任何标签

通用选择器，将匹配任何标签。不建议使用，IE有些版本不支持，大网站增加客户端负担。

效率不高，如果页面上的标签越多，效率越低，所以页面上不能出现这个选择器。

举例：

```css
*{
	margin-left:0px;
	margin-top:0px;
}

```

效果：

![](http://img.smyhvae.com/2015-10-03-css-09.png)

## CSS的几种高级选择器

**高级选择器：**

 - 后代选择器：用空格隔开
 - 交集选择器：选择器之间紧密相连
 - 并集选择器（分组选择器）：用逗号隔开
 - 伪类选择器

下面详细讲一下这几种高级（扩展）选择器。

### 1、后代选择器: 定义的时候用空格隔开

对于`E F`这种格式，表示**所有属于E元素后代的F元素**，有这个样式。空格就表示后代。

后代选择器，就是一种平衡：共性、特性的平衡。当要把**某一个部分的所有的什么**，进行样式改变，就要想到后代选择器。

后代选择器，描述的是祖先结构。

看定义可能有点难理解，我们来看例子吧。

举例1：

```html
	<style type="text/css">
		.div1 p{
			color:red;
		}
	</style>
```

空格就表示后代。`.div1 p` 表示`.div1`的后代所有的`p`。

这里强调一下：这两个标签不一定是连续紧挨着的，只要保持一个后代的关联即可。也就是说，选择的是后代，不一定是儿子。

举例：

```html
	<style type="text/css">
		h3 b i{
			color:red ;
		}
	</style>
```

上方代码的意思是说：定义了`<h3>`标签中的`<b>`标签中的`<i>`标签的样式。
同理：h3和b和i标签不一定是连续紧挨着的，只要保持一个后代的关联即可。

效果：

![](http://img.smyhvae.com/2015-10-03-css-11.png)

或者还有下面这种写法：

![](http://img.smyhvae.com/2015-10-03-css-12.png)

上面的这种写法，`<h3>`标签和`<i>`标签并不是紧挨着的，但他们保持着一种后代关系。

还有下面这种写法：（含类选择器、id选择器都是可以的）

![](http://img.smyhvae.com/2015-10-03-css-13.png)

我们在开头说了：**后代选择器，描述的是一种祖先结构**。我们举个例子来说明这句话：

```html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<style type="text/css">
		div div p{
			color: red;
		}
	</style>
</head>
<body>
	<div>
		<div class="div2">
			<div class="div3">
				<div class="div4">
					<p>我是什么颜色？</p>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
```

上面css中的`div div p`，也能使文字的颜色变红。通过浏览器的审查元素，我们可以看到	p元素的祖先列表：

![](http://img.smyhvae.com/20170711_1836.png)

讲到这里，我们再提一个sublme的快捷键。

在sublime中输入`p#haha`，按tab键后，会生成`<p id="haha"></p>`。

在sublime中输入`p.haha`，按tab键后，会生成`<p class="haha"></p>`。

### 2、交集选择器：定义的时候紧密相连

定义交集选择器的时候，两个选择器之间紧密相连。一般是以标签名开头，比如`div.haha`，再比如`p.special`。

如果后一个选择器是类选择器，则写为`div.special`；如果后一个选择器id选择器，则写为`div#special`。

来看下面这张图就明白了：

![](http://img.smyhvae.com/20170711_1851.png)


```css
h3.special{
	color:red;
}
```

选择的元素要求同时满足两个条件：必须是h3标签，然后必须是special标签。

举例：

```html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>交集选择器测试</title>
	<style type="text/css">
		h3.special {
			color: red;
		}

	</style>
</head>
<body>
	<h3 class="special zhongyao">标题1</h3>
	<h3 class="special">我也是标题</h3>
	<p>我是段落</p>
</body>
</html>
```

效果如下：

![](http://img.smyhvae.com/20170711_1852.png)

注意，交集选择器没有空格。所以，没有空格的`div.red`（交集选择器）和有空格的`div .red`（后代选择器）不是一个意思。

交集选择器可以连续交：（一般不要这么写）

```css
h3.special.zhongyao{
	color:red;
}
```

上面这种写法，是 IE7 开始兼容的，IE6 不兼容。

### 3、并集选择器：定义的时候用逗号隔开

三种基本选择器都可以放进来。

举例：

```css
p,h1,#mytitle,.one{
	color:red;
}
```

效果：

![](http://img.smyhvae.com/2015-10-03-css-10.png)

## 一些 CSS3 选择器

> 所有的 CSS3 选择器，我们放在 CSS3 的内容里介绍。本文暂时先接触一部分。

### 浏览器的兼容性问题

> 我们可以用`IETester`这个软件测一下CSS在各个版本IE浏览器上的显示效果。

IE： 微软的浏览器，随着操作系统安装的。所以每个windows都有IE浏览器。各版本如下：

- windows xp 	操作系统安装的IE6
- windows vista 操作系统安装的IE7
- windows 7 	操作系统安装的IE8
- windows 8 	操作系统安装的IE9
- windows10 	操作系统安装的edge

浏览器兼容问题，要出，就基本上就是出在IE6、7身上，这两个浏览器是非常低级的浏览器。

为了测试浏览器CSS 3的兼容性，我们可以在网上搜"css3 机器猫"关键字，然后在不同的浏览器中打开如下链接：

- <http://www1.pconline.com.cn/pcedu/specialtopic/css3-doraemon/>

测试结果如下：

![](http://img.smyhvae.com/20170711_1939.png)

我们可以在[百度统计](http://tongji.baidu.com/data/)里查看浏览器的市场占有率：

- IE9	5.94%
- IE8 21.19%
- IE7 4.79%
- IE6 4.11%

我们可以在<http://html5test.com/results/desktop.html>中查看

![](http://img.smyhvae.com/20170711_1948.png)

我们要知道典型的IE6兼容问题（面试要问），但是做项目我们兼容到IE8即可。不解决IE8以下的兼容问题，目的在于：培养更高的兴趣和眼光，别天天的跟IE6较劲。

我们可以用「IETester」软件看看css在各个浏览器中的显示效果。

### 1.子代选择器，用符号`>`表示

> IE7开始兼容，IE6不兼容。

```css
div>p{
	color:red;
}
```

div的儿子p。和div的后代p的截然不同。

能够选择：

```html
	<div>
		<p>我是div的儿子</p>
	</div>
```

不能选择：

```html
	<div>
		<ul>
			<li>
				<p>我是div的重孙子</p>
			</li>
		</ul>
	</div>
```

### 2.序选择器

> IE8开始兼容；IE6、7都不兼容

设置无序列表`<ul>`中的第一个`<li>`为红色：

```html
	<style type="text/css">
		ul li:first-child{
			color:red;
		}
	</style>
```

设置无序列表`<ul>`中的最后一个`<li>`为红色：

```css
		ul li:last-child{
			color:blue;
		}
```

序选择器还有更复杂的用法，以后再讲。

由于浏览器的更新需要过程，所以现在如果公司还要求兼容IE6、7，那么就要自己写类名：

```html
	<ul>
		<li class="first">项目</li>
		<li>项目</li>
		<li>项目</li>
		<li>项目</li>
		<li>项目</li>
		<li>项目</li>
		<li>项目</li>
		<li>项目</li>
		<li>项目</li>
		<li class="last">项目</li>
	</ul>
```

用类选择器来选择第一个或者最后一个：

```html
		ul li.first{
			color:red;
		}

		ul li.last{
			color:blue;
		}
```

### 3.下一个兄弟选择器

> IE7开始兼容，IE6不兼容。

`+`表示选择下一个兄弟

```html
	<style type="text/css">
		h3+p{
			color:red;
		}
	</style>
```

上方的选择器意思是：选择的是h3元素后面紧挨着的第一个兄弟。

```html
    <h3>我是一个标题</h3>
	<p>我是一个段落</p>
	<p>我是一个段落</p>
	<p>我是一个段落</p>
	<h3>我是一个标题</h3>
	<p>我是一个段落</p>
	<p>我是一个段落</p>
	<p>我是一个段落</p>
	<h3>我是一个标题</h3>
	<p>我是一个段落</p>
	<p>我是一个段落</p>
	<p>我是一个段落</p>
	<h3>我是一个标题</h3>
```

效果如下：

![](http://img.smyhvae.com/20170711_1950.png)

这种选择器作用不大。





## 伪类（伪类选择器）


**伪类**：同一个标签，根据其**不同的种状态，有不同的样式**。这就叫做“伪类”。伪类用冒号来表示。


比如div是属于box类，这一点很明确，就是属于box类。但是a属于什么类？不明确。因为需要看用户点击前是什么状态，点击后是什么状态。所以，就叫做“伪类”。



### 静态伪类和动态伪类


伪类选择器分为两种。

（1）**静态伪类**：只能用于**超链接**的样式。如下：

- `:link` 超链接点击之前
- `:visited` 链接被访问过之后

PS：以上两种样式，只能用于超链接。

（2）**动态伪类**：针对**所有标签**都适用的样式。如下：

- `:hover` “悬停”：鼠标放到标签上的时候
- `:active`	“激活”： 鼠标点击标签，但是不松手时。
- `:focus` 是某个标签获得焦点时的样式（比如某个输入框获得焦点）


## 超链接a标签

### 超链接的四种状态


a标签有4种伪类（即对应四种状态），要求背诵。如下：

- `:link`  	“链接”：超链接点击之前
- `:visited` “访问过的”：链接被访问过之后
- `:hover`	“悬停”：鼠标放到标签上的时候
- `:active`	“激活”： 鼠标点击标签，但是不松手时。


对应的代码如下：

```html
<style type="text/css">
	/*让超链接点击之前是红色*/
	a:link{
		color:red;
	}

	/*让超链接点击之后是绿色*/
	a:visited{
		color:orange;
	}

	/*鼠标悬停，放到标签上的时候*/
	a:hover{
		color:green;
	}

	/*鼠标点击链接，但是不松手的时候*/
	a:active{
		color:black;

</style>
```


记住，在css中，这四种状态**必须按照固定的顺序写**：

> a:link 、a:visited 、a:hover 、a:active

如果不按照顺序，那么将失效。“爱恨准则”：love hate。必须先爱，后恨。

看一下这四种状态的动图效果：

![](http://img.smyhvae.com/20180113_2239.gif)

### 超链接的美化

问：既然`a{}`定义了超链的属性，和`a:link{}`定义了超链点击之前的属性，那这两个有啥区别呢？

答：

**`a{}`和`a:link{}`的区别：**

 - `a{}`定义的样式针对所有的超链接(包括锚点)
 - `a:link{}`定义的样式针对所有写了href属性的超链接(不包括锚点)

超链接a标签在使用的时候，比较难。因为不仅仅要控制a这个盒子，也要控制它的伪类。

我们一定要将a标签写在前面，将`:link、:visited、:hover、:active`这些伪类写在后面。

针对超链接，我们来举个例子：

![](http://img.smyhvae.com/20170810_2235.gif)


为了实现上面这个效果，完整版代码如下：

```html
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>Document</title>
	<style type="text/css">
		*{
			margin: 0;
			padding: 0;
		}
		.nav{
			width: 960px;
			height: 50px;
			border: 1px solid red;
			margin: 100px auto;
		}
		.nav ul{
			/*去掉小圆点*/
			list-style: none;
		}
		.nav ul li{
			float: left;
			width: 120px;
			height: 50px;
			/*让内容水平居中*/
			text-align: center;
			/*让行高等于nav的高度，就可以保证内容垂直居中*/
			line-height: 50px;
		}
		.nav ul li a{
			display: block;
			width: 120px;
			height: 50px;
		}
		/*两个伪类的属性，可以用逗号隔开*/
		.nav ul li a:link , .nav ul li a:visited{
			text-decoration: none;
			background-color: purple;
			color:white;
		}
		.nav ul li a:hover{
			background-color: orange;
		}
	</style>
</head>
<body>
	<div class="nav">
		<ul>
			<li><a href="#">网站栏目</a></li>
			<li><a href="#">网站栏目</a></li>
			<li><a href="#">网站栏目</a></li>
			<li><a href="#">网站栏目</a></li>
			<li><a href="#">网站栏目</a></li>
			<li><a href="#">网站栏目</a></li>
			<li><a href="#">网站栏目</a></li>
			<li><a href="#">网站栏目</a></li>
		</ul>
	</div>
</body>
</html>
```

上方代码中，我们发现，当我们在定义`a:link`和 `a:visited`这两个伪类的时候，如果它们的属性相同，我们其实可以写在一起，用逗号隔开就好，摘抄如下：

```css
		.nav ul li a{
			display: block;
			width: 120px;
			height: 50px;
		}
		/*两个伪类的属性，可以用逗号隔开*/
		.nav ul li a:link , .nav ul li a:visited{
			text-decoration: none;
			background-color: purple;
			color:white;
		}
		.nav ul li a:hover{
			background-color: orange;
		}
```

如上方代码所示，最标准的写法，就是把link、visited、hover这三个伪类都要写。但是前端开发工程师在大量的实践中，发现不写link、visited也挺兼容。写法是：

> a:link、a:visited都是可以省略的，简写在a标签里面。也就是说，a标签涵盖了link、visited的状态（前提是都具有了相同的属性）。写法如下：


```css
		.nav ul li a{
			display: block;
			width: 120px;
			height: 50px;
			text-decoration: none;
			background-color: purple;
			color:white;
		}
		.nav ul li a:hover{
			background-color: orange;
		}

```

当然了，在写`a:link`、`a:visited`这两个伪类的时候，要么同时写，要么同时不写。如果只写`a`属性和`a:link`属性，不规范。

## 动态伪类举例

我们在第一段中描述过，下面这三种动态伪类，针对所有标签都适用。

- `:hover` “悬停”：鼠标放到标签上的时候
- `:active`	“激活”： 鼠标点击标签，但是不松手时。
- `:focus` 是某个标签获得焦点时的样式（比如某个输入框获得焦点）

我们不妨来举下例子。

举例1：

```html
  <style type="text/css">
  /*
	伪类选择器：动态伪类
  */

   /*
	让文本框获取焦点时：
	边框：#FF6F3D这种橙色
	文字：绿色
	背景色：#6a6a6a这种灰色
   */
	input:focus{
		border:3px solid #FF6F3D;
		color:white;
		background-color:#6a6a6a;
	}

	/*
	鼠标放在标签上时显示蓝色
    */
	label:hover{
		color:blue;
	}

	/*
	点击标签鼠标没有松开时显示红色
    */
	label:active{
		color:red;
	}

  </style>
```

效果：

![](http://img.smyhvae.com/2015-10-03-css-02.gif)

利用这个`hover`属性，我们同样对表格做一个样式的设置：
表格举例：

```html
<!doctype html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta name="Generator" content="EditPlus®">
  <meta name="Author" content="">
  <meta name="Keywords" content="">
  <meta name="Description" content="">
  <title>Document</title>
  <style type="text/css">

	/*整个表格的样式*/
  	table{
		width: 300px;
		height: 200px;
		border: 1px solid blue;
		/*border-collapse属性：对表格的线进行折叠*/
		border-collapse: collapse;
  	}

	/*鼠标悬停时，让当前行显示#868686这种灰色*/
  	table tr:hover{
  		background: #868686;
  	}

	/*每个单元格的样式*/
  	table td{
  		border:1px solid red;
  	}

  </style>
 </head>
 <body>

  <table>
  <tr>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
  </tr>
  <tr>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
  </tr>
  <tr>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
  </tr>
  </table>

 </body>
</html>
```

效果：

![](http://img.smyhvae.com/2015-10-03-css-04.gif)

